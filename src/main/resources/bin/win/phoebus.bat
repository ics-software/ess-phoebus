@REM Phoebus launcher for Windows
@REM Uses a JDK that's located next to this folder,
@REM otherwise assumes JDK is on the PATH

@cd %~P0

@IF EXIST "%~P0%..\jdk" (
    set JAVA_HOME=%~P0%..\jdk
    @path %JAVA_HOME%\bin
    @ECHO Found JDK %JAVA_HOME%
)

@java -version

SET JAR=ess-cs-studio-phoebus-${project.version}-win.jar

@REM To get one instance, use server mode
@java -Dcom.sun.webkit.useHTTP2Loader=false -jar %JAR% -server 4918 -logging logging.properties %*
