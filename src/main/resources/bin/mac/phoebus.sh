#!/usr/bin/env bash
#
BASEDIR=$(dirname "$0")
PHOEBUS_OPTS="-server 4918 -logging $BASEDIR/logging.properties"
PHOEBUS_JAR=$BASEDIR/ess-cs-studio-phoebus-${project.version}-mac.jar
JAVA_OPTS="$JAVA_OPTS -Dprism.lcdtext=false -Dcom.sun.webkit.useHTTP2Loader=false"

java $JAVA_ARGS $JAVA_OPTS -jar $PHOEBUS_JAR $PHOEBUS_OPTS "$@"
